declare module 'mochawesome-merge' {
    interface Options {
      files: string[];
      reportDir?: string;
      reportFilename?: string;
      saveJson?: boolean;
      saveHtml?: boolean;
      inlineAssets?: boolean;
    }
    
    function merge(options: Options): Promise<any>;
  
    export = merge;
  }
  