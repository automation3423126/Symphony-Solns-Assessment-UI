declare module 'cypress-mochawesome-reporter/plugin' {
  function install(on: Cypress.PluginEvents, config: Cypress.PluginConfigOptions): void;
  export = install;
}
