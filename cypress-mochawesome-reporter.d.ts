declare module 'cypress-mochawesome-reporter/plugin' {
    const plugin: any;
    export = plugin;
  }

  // types/cypress-mochawesome-reporter.d.ts
declare module 'cypress-mochawesome-reporter' {
  // Add your type definitions here
  export function reporterFunction(): void;
}

 