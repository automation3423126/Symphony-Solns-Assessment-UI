const { defineConfig } = require('cypress');
const merge = require('lodash/merge');
//const merge = 'mochawesome-merge';
const mochawesome = 'cypress-mochawesome-reporter/plugin';


console.log(typeof merge); // Should print 'function'

//const generator = 'mochawesome-report-generator';

module.exports = defineConfig({
  e2e: {
    baseUrl: 'https://www.saucedemo.com',
    pageLoadTimeout: 150000,
    chromeWebSecurity: false,

    setupNodeEvents(on, config) {
      // implement node event listeners here
      // require('cypress-mochawesome-reporter/plugin')(on);
      // return config;
      on('after:run', (results) => {
        // Your logic here
        console.log('Results:', results);
},
      )},

  reporter: 'mocha-multi-reporters',
  reporterOptions: {
    reportDir: 'cypress/reports',
    configFile: 'reporter-config.json',
    overwrite: false,
    html: true,
    json: true,
    charts: true,
    //embeddedScreenshots: true,
    inlineAssets: true,
  
  },

  screenshotsFolder: 'cypress/screenshots',
},
});

